#!/bin/bash

# Insure the system is up to date before installing new software
sudo apt-get update && apt-get upgrade

# Add apt repositorys that are required to install the software
sudo add-apt-repository ppa:gnome-terminator
sudo add-apt-repository ppa:papirus/papirus

# Grab information from the new repositorys
sudo apt-get update

# Install new software
sudo apt-get install redshift
sudo apt-get install terminator
sudo apt-get install papirus-icon-theme
sudo apt-get install vlc
sudo apt-get install vim-gtk3
sudo apt-get install git

# Grab my dotfiles
cd ~
git clone https://github.com/ThomasSaunders/dotfiles.git

# Generate ssh key for git
ssh-keygen -t rsa

# Set git global information
git config --global user.email "thomassaunders379@gmail.com"
git config --global user.name "Thomas Saunders"
